---
title: "Extracting HTTP Headers with tcpdump"
date: 2019-12-30T22:06:51+02:00
draft: false
toc: false
images:
tags:
  - tcpdump
  - http
  - headers
  - linux
---
There are times when you need to inspect and troubleshoot which headers are being received by web server. There's a neat `tcpdump` oneliner lurking on the internet which extracts headers from incoming network traffic:

```bash
stdbuf -oL -eL /usr/sbin/tcpdump -A -s 10240 "tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)" | egrep -a --line-buffered ".+(GET |HTTP\/|POST )|^[A-Za-z0-9-]+: " | perl -nle 'BEGIN{$|=1} { s/.*?(GET |HTTP\/[0-9.]* |POST )/\n$1/g; print }'
```

And here's an example output that it provides:

```bash
HTTP/1.1 200 OK
Server: nginx
Date: Tue, 31 Dec 2019 12:48:45 GMT
Content-Type: text/plain
Content-Length: 123
Connection: close
Cache-Control: no-store must-revalidate,max-age=0
Content-Encoding: gzip
Last-Modified: Mon, 23 Dec 2019 09:04:56 GMT
Accept-Ranges: bytes
ETag: W/"72f8efb70b9d51:0"
Vary: Accept-Encoding
```
