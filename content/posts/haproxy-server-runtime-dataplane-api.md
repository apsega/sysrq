---
title: "Managing HAProxy backend servers states with Data Plane API"
date: 2020-02-17T21:10:13+02:00
draft: false
tags:
  - haproxy
---

HAProxy's Data Plane API is a neat tool to manage its configuration and it has some great [docs](https://www.haproxy.com/documentation/hapee/1-9r1/configuration/dataplaneapi/), too. But in case we only need to change backend's server state (ready/drain/maint) it might be overkill using all of those transactions, version numbers and configuration dumps.

Luckily, there's actually a runtime API function that's not mentioned in the documentation, but according to this [code](https://github.com/haproxytech/dataplaneapi/commit/a79a41935be1b8d7a8478e00c2528b3682baf7c8), it's easy to use. Here's how:

### Check backend server's state

```bash
curl --user dataplaneapi:mypassword \
  --header "Content-Type: application/json" \
  --request GET "http://localhost:5555/v2/services/haproxy/runtime/servers/$SERVER_NAME?backend=$BACKEND_NAME" | jq .
```
Output:
```json
{
  "address": "10.0.0.11",
  "admin_state": "ready",
  "id": "1",
  "name": "$SERVER_NAME",
  "operational_state": "up",
  "port": 80
}
```

### Change backend server's state

Prepare payload JSON file:
```json
{
  "admin_state":"maint",
  "operational_state":"up"
}
```

`PUT` JSON file to HAProxy Data Plane API:
```bash
curl --data @payload.json \
  --user dataplaneapi:mypassword \
  --header "Content-Type: application/json" \
  --request PUT "http://localhost:5555/v2/services/haproxy/runtime/servers/$SERVER_NAME?backend=$BACKEN_NAME" | jq '.'
```

Output:
```json
{
  "address": "10.0.0.11",
  "admin_state": "maint",
  "id": "1",
  "name": "$SERVER_NAME",
  "operational_state": "down",
  "port": 80
}
```