---
title: "Nginx: mirroring part of the traffic"
date: 2020-01-01T20:34:47+02:00
draft: false
toc: false
images:
tags:
  - nginx
  - linux
  - traffic
  - shadowing
  - mirroring
---
There's a need to mirror traffic to other backends for various purposes and it's easy to achieve with Nginx `ngx_http_mirror_module` (intrduced in 1.13.4):

``` nginx
location / {
    mirror /mirror;
    proxy_pass http://backend;
}

location = /mirror {
    internal;
    proxy_pass http://test_backend$request_uri;
}
```

But what if we don't want to mirror all of the traffic, only part of it? That's where `ngx_http_split_clients_module` comes in. Below is a configuration example to mirror only 50% of the traffic based on clients IP addresses (`remote_addr`):

``` nginx
http {

    split_clients "${remote_addr}" $half_traffic {
                   50%               test_mirror;
                   *                  "";
    }

    upstream test_mirror {
        server 10.0.0.1:8080;
    }

    server {
        location / {
            mirror /mirror;
            proxy_pass http://backend;
        }

        location = /mirror {
            internal;
            proxy_pass http://$half_traffic$request_uri;
        }

    }

}
```