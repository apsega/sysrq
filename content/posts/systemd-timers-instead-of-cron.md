---
title: "How to use systemd Timers instead of cron"
date: 2020-04-04T21:02:34+03:00
draft: false
tags:
  - linux
  - systemd
---
There are some benefits of using systemd Timers instead of cron:
* Centralized place to look for logs (`journalctl -xefu <service>`);
* Scheduled tasks run like systemd service;
* Detailed log about scheduled tasks (`systemctl list-timers --all`).

Or if you have OS flavor that doesn't support cron, like CoreOS, you're doomed to use systemd Timers.

## Create Service and Timer manifests

To setup scheduled tasks, you'll need to create systemd service manifest named `notifier`.

In example, `/etc/systemd/system/notifier.service`:

```systemd
[Unit]
Description=Notifies about <something>

[Service]
Type=oneshot
ExecStart=/usr/bin/sh -c 'docker run --rm random-notifier:1.1 --some-flags'
```

Then we need to create a Timer with same name - `/etc/systemd/system/barbora-notifier.timer`:

```systemd
[Unit]
Description=Run random-notifier.service every 10 minutes

[Timer]
OnCalendar=*:0/10
RandomizedDelaySec=120
```

This timer will spinup Docker container every 10 minutes, adding random delay up to 2 minutes.

## Start Timer

To start timer, first reload systemctl daemon:

```sh
systemctl daemon-reload
```

And start Timer:

```sh
systemctl start random-notifier.timer
```

## Additional information

You can list all timers and get additional information:

```sh
$ systemctl list-timers --all
NEXT                         LEFT          LAST                         PASSED       UNIT                         ACTIVATES
Sat 2020-04-04 18:21:04 UTC  6min left     Sat 2020-04-04 18:10:23 UTC  3min 57s ago random-notifier.timer        random-notifier.service
Sat 2020-04-04 20:02:15 UTC  1h 47min left Sat 2020-04-04 08:02:15 UTC  10h ago      rkt-gc.timer                 rkt-gc.service
Sun 2020-04-05 00:00:00 UTC  5h 45min left Sat 2020-04-04 00:00:06 UTC  18h ago      logrotate.timer              logrotate.service
Sun 2020-04-05 12:25:45 UTC  18h left      Sat 2020-04-04 12:25:45 UTC  5h 48min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service
n/a                          n/a           n/a                          n/a          update-engine-stub.timer     update-engine-stub.service

5 timers listed.
```

To check your scheduled tasks logs:

```sh
journalctl -xefu random-notifier.service
```
