---
title: "Telia Internet and IPTV (Lithuanian Zebra) on Ubiquiti Edgerouter 12P"
date: 2021-01-14T14:16:50+02:00
draft: false
tags:
  - edgeos
  - edgerouter
  - iptv
---

Use VLAN aware Switch feature on Ubiquiti Edgerouter 12P to utilize maximum ethernet speed.

Ports eth0 for ISP incoming Internet and eth1 for IPTV.

The catch is to have `pvid 1` and `vid 6` on WAN eth port (in this case 0) and `pvid 6` on IPTV LAN port (in this case 1). All other ports for LAN should have `pvid 2`.

Full configuration:

```c#
firewall {
    all-ping enable
    broadcast-ping disable
    ipv6-name WANv6_IN {
        default-action drop
        description "WAN inbound traffic forwarded to LAN"
        enable-default-log
        rule 10 {
            action accept
            description "Allow established/related sessions"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
    }
    ipv6-name WANv6_LOCAL {
        default-action drop
        description "WAN inbound traffic to the router"
        enable-default-log
        rule 10 {
            action accept
            description "Allow established/related sessions"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
        rule 30 {
            action accept
            description "Allow IPv6 icmp"
            protocol ipv6-icmp
        }
        rule 40 {
            action accept
            description "allow dhcpv6"
            destination {
                port 546
            }
            protocol udp
            source {
                port 547
            }
        }
    }
    ipv6-receive-redirects disable
    ipv6-src-route disable
    ip-src-route disable
    log-martians enable
    name WAN_IN {
        default-action drop
        description "WAN to internal"
        rule 10 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
    }
    name WAN_LOCAL {
        default-action drop
        description "WAN to router"
        rule 10 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
    }
    receive-redirects disable
    send-redirects enable
    source-validation disable
    syn-cookies enable
}
interfaces {
    ethernet eth0 {
        description Internet
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth1 {
        description IPTV
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth2 {
        description Local
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth3 {
        description Local
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth4 {
        description Local
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth5 {
        description Local
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth6 {
        description Local
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth7 {
        description Local
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth8 {
        address 192.168.1.1/24
        description Local2
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    ethernet eth9 {
        address dhcp
        description Local
        duplex auto
        firewall {
            in {
                ipv6-name WANv6_IN
            }
            local {
                ipv6-name WANv6_LOCAL
            }
        }
        poe {
            output off
        }
        speed auto
    }
    loopback lo {
    }
    switch switch0 {
        description Local
        mtu 1500
        switch-port {
            interface eth0 {
                vlan {
                    pvid 1
                    vid 6
                }
            }
            interface eth1 {
                vlan {
                    pvid 6
                }
            }
            interface eth2 {
                vlan {
                    pvid 2
                }
            }
            interface eth3 {
                vlan {
                    pvid 2
                }
            }
            interface eth4 {
                vlan {
                    pvid 2
                }
            }
            interface eth5 {
                vlan {
                    pvid 2
                }
            }
            interface eth6 {
                vlan {
                    pvid 2
                }
            }
            interface eth7 {
                vlan {
                    pvid 2
                }
            }
            vlan-aware enable
        }
        vif 1 {
            address dhcp
            description "Internet VLAN"
            firewall {
                in {
                    ipv6-name WANv6_IN
                    name WAN_IN
                }
                local {
                    ipv6-name WANv6_LOCAL
                    name WAN_LOCAL
                }
            }
        }
        vif 2 {
            address 192.168.2.1/24
            description "LAN VLAN"
            mtu 1500
        }
        vif 6 {
            description IPTV
            mtu 1500
        }
    }
}
port-forward {
    auto-firewall enable
    hairpin-nat enable
    lan-interface switch0
    wan-interface switch0.1
}
service {
    dhcp-server {
        disabled false
        hostfile-update disable
        shared-network-name LAN1 {
            authoritative enable
            subnet 192.168.1.0/24 {
                default-router 192.168.1.1
                dns-server 192.168.1.1
                lease 86400
                start 192.168.1.38 {
                    stop 192.168.1.243
                }
            }
        }
        shared-network-name LAN2 {
            authoritative enable
            subnet 192.168.2.0/24 {
                default-router 192.168.2.1
                dns-server 192.168.2.67
                lease 86400
                start 192.168.2.38 {
                    stop 192.168.2.243
                }
            }
        }
        static-arp disable
        use-dnsmasq disable
    }
    dns {
        forwarding {
            cache-size 10000
            listen-on eth8
            listen-on switch0.2
            name-server 192.168.2.1
        }
    }
    gui {
        http-port 80
        https-port 443
        older-ciphers enable
    }
    nat {
        rule 5010 {
            description "masquerade for WAN"
            log disable
            outbound-interface switch0.1
            protocol all
            type masquerade
        }
    }
    ssh {
        port 22
        protocol-version v2
    }
}
```