---
title: "GitLab with Ansible"
date: 2020-01-25T14:06:23+02:00
draft: false
---

Even free GitLab plan lets you use its [Runners](https://docs.gitlab.com/runner/) functionality which makes perfect to integrate it with Ansible and have a minimal CI for syntax and lint checking. 

Just place `.gitlab-ci.yml` file in your Ansible repo that's hosted on GitLab, i.e. file tree:

```
.gitignore
.gitlab-ci.yml    # <--- our configuration for Runners
group_vars/
host_vars/
roles/
README.md
ansible.cfg
hosts
loadbalancers.yml
webserver.yml
site.yml
```

Here are an example contents of `.gitlab-ci.yml` two have two CI steps, one for lint check and another one for syntax check:

```yaml
---
# -*- coding: utf-8 -*-
before_script:
  - apt-get update -qy
  - apt-get install -y python-dev python-pip
  - git submodule update --init
  - pip install --upgrade ansible ansible-lint
  - ansible --version
  - ansible-lint --version

stages:
  - ansible-lint
  - ansible-syntax-check

ansible-lint:
  stage: ansible-lint
  script:
    - ansible-lint site.yml

ansible-syntax-check:
  stage: ansible-syntax-check
  script:
    - ansible-playbook --inventory hosts --syntax-check site.yml

```

That's it! Now on every push event Gitlab runners will take care of Ansible CI.