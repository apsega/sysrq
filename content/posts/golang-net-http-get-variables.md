---
title: "Golang: variables in net/http URL string"
date: 2020-02-15T20:58:13+02:00
draft: false
tags:
  - golang
---

While learning to code Go, I needed to have variables in net/http package's URL string. There weren't any resources that led to simple examples if it's possible to spin a loop and call different external URLs. Well, It's possible and very easy to achieve.

Here's an example code on how to `GET` external URL:

```go
resp, err := http.Get("https://sysrq.tech")
```

If we want to parameterize URL string, we can do it this way:

```go
resp, err := http.Get(scheme + "://" + host + "/" + path)
```

A full example of how to get a website body:

```go
package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {

	scheme := "https"
	host := "sysrq.tech"
	path := "docker-misleading-containers-memory-usage"

	resp, err := http.Get(scheme + "://" + host + "/" + path)

	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	fmt.Println(string(body))

}
```